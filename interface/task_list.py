from typing import Callable

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QLabel, QListWidget, QVBoxLayout, QDialog, QDialogButtonBox, QTextEdit, \
    QComboBox, QPushButton, QListWidgetItem, QMenu, QAction, QAbstractItemView

from internal.task import Task
from models.models import TaskStatus, Task as DbTask
from utils.utils import db_session


class TaskCreationModal(QDialog):
    """
    Модальное окно для создания новой задачи
    """

    def __init__(self, parent, status: TaskStatus):
        super().__init__(parent=parent)

        self.setWindowTitle("Создание задачи")
        action_button = QDialogButtonBox.Ok | QDialogButtonBox.Cancel

        self.button_box = QDialogButtonBox(action_button)
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        self.layout.addWidget(QLabel('Название задачи'))
        self.name_input = QTextEdit()
        self.layout.addWidget(self.name_input)
        self.layout.addWidget(QLabel('Статус задачи'))
        self.status_input = QComboBox()
        self.layout.addWidget(self.status_input)

        self.layout.addWidget(self.button_box)
        self.setLayout(self.layout)

        self.status_input.addItem(status.value)

    def create_task(self):
        """
        Создание задачи по данным, которые ввёл пользователь
        :return:
        """
        with db_session() as session:
            Task.create(session, self.name_input.toPlainText(), self.status_input.currentText())


class TaskDeleteModal(QDialog):
    """
    Подтверждение удаления задачи
    """

    def __init__(self, parent, task: DbTask):
        super().__init__(parent=parent)

        self.task = task
        self.setWindowTitle("Удаление задачи")
        action_button = QDialogButtonBox.Ok | QDialogButtonBox.Cancel

        self.button_box = QDialogButtonBox(action_button)
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        self.layout.addWidget(QLabel(f'Вы действительно хотите удалить задачу?'))

        self.layout.addWidget(self.button_box)
        self.setLayout(self.layout)

    def delete_task(self):
        with db_session() as session:
            Task.delete(session, self.task.id)


class TaskChangeStatusModal(QDialog):
    def __init__(self, parent, task: DbTask):
        super().__init__(parent=parent)

        self.task = task
        self.setWindowTitle("Смена статуса")
        action_button = QDialogButtonBox.Ok | QDialogButtonBox.Cancel

        self.button_box = QDialogButtonBox(action_button)
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        self.layout.addWidget(QLabel(f'Выберите новый статус'))

        self.status_input = QComboBox()
        self.layout.addWidget(self.status_input)

        for status in TaskStatus:
            self.status_input.addItem(status.value)

        self.layout.addWidget(self.button_box)
        self.setLayout(self.layout)

    def change_status(self):
        with db_session() as session:
            Task.update(session, self.task.id, status=self.status_input.currentText(), name=None)


class TaskList:
    """
    Класс для работы со списками задач
    """

    def __init__(self, parent, base_widget: QWidget, widget_label: QLabel, list_widget: QListWidget,
                 layout: QVBoxLayout, status: TaskStatus,
                 start_task_callback: Callable[[DbTask], None],
                 refresh_all_callback: Callable):
        self.refresh_all_callback = refresh_all_callback
        self.start_task_callback = start_task_callback

        self.parent = parent
        self.status = status
        self.layout = layout
        self.list_widget = list_widget
        self.widget_label = widget_label
        self.base_widget = base_widget

        self.refresh()

        self.list_widget.setEditTriggers(QAbstractItemView.NoEditTriggers)

        self.list_widget.itemDoubleClicked.connect(self.item_clicked)

        self.list_widget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.list_widget.customContextMenuRequested.connect(self.create_context_menu)

    def create_context_menu(self, position):
        pop_menu = QMenu()

        create_action = QAction("Создать", self.list_widget)
        delete_action = QAction("Удалить", self.list_widget)
        move_action = QAction("Переместить", self.list_widget)

        pop_menu.addAction(create_action)
        if item := self.list_widget.itemAt(position):
            pop_menu.addAction(delete_action)
            pop_menu.addAction(move_action)
        self.current_item = item

        create_action.triggered.connect(self.create_new)
        delete_action.triggered.connect(self.delete_item)
        move_action.triggered.connect(self.change_item_status)
        pop_menu.exec_(self.list_widget.mapToGlobal(position))

    def change_item_status(self):
        item = self.current_item
        dialog = TaskChangeStatusModal(self.parent, item.task)
        if dialog.exec_():
            dialog.change_status()
            self.refresh_all_callback()

    def delete_item(self):
        item = self.current_item
        dialog = TaskDeleteModal(self.parent, item.task)
        if dialog.exec_():
            dialog.delete_task()
            self.refresh()

    def item_clicked(self, item):
        self.start_task_callback(item.task)

    def create_new(self):
        dialog = TaskCreationModal(self.parent, self.status)
        if dialog.exec_():
            dialog.create_task()
            self.refresh()

    def refresh(self):
        self.list_widget.clear()
        with db_session() as session:
            tasks = Task.get_list(session, self.status)
            for task in tasks:
                item = QListWidgetItem(task.name, self.list_widget)
                item.setToolTip('Кликните дважды для старта задачи')
                item.task = task
                self.list_widget.addItem(item)

