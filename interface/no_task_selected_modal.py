from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout, QLabel


class NoTaskSelectedModal(QDialog):
    """
    Предупреждение, если пользователь не выбрал задачу
    """

    def __init__(self, parent):
        super().__init__(parent=parent)

        self.setWindowTitle("Задача не выбрана")
        action_button = QDialogButtonBox.Ok

        self.button_box = QDialogButtonBox(action_button)
        self.button_box.accepted.connect(self.accept)

        self.layout = QVBoxLayout()
        self.layout.addWidget(QLabel('Задача не выбрана'))

        self.layout.addWidget(self.button_box)
        self.setLayout(self.layout)

