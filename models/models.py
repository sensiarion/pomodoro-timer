import enum
from typing import List

from sqlalchemy import func, Column, Integer, DateTime, Boolean, ForeignKey, Enum, Text
from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base
from sqlalchemy.orm import relationship

Base: DeclarativeMeta = declarative_base()
metadata = Base.metadata


def fresh_timestamp():
    """Небольшой хелпер для работы с timestamp на уровне ОРМа
    по сути "передает команду БД использовать нативную функцию NOW()"
    """
    return func.now()


class Settings(Base):
    __tablename__ = 'settings'
    id = Column(Integer, primary_key=True, autoincrement=True)

    tomato_duration = Column(Integer, nullable=False)
    is_need_notificate = Column(Boolean, nullable=False, default='true')
    updated_at = Column(DateTime, default=fresh_timestamp(), onupdate=fresh_timestamp())


class TomatoSession(Base):
    __tablename__ = 'tomato_sessions'
    id = Column(Integer, primary_key=True, autoincrement=True)

    duration = Column(Integer, nullable=False)
    ended_at = Column(DateTime, default=fresh_timestamp())
    created_at = Column(DateTime, default=fresh_timestamp())


class TaskStatus(enum.Enum):
    queue = 'queue'
    in_progress = 'in_progress'
    done = 'done'


class Task(Base):
    __tablename__ = 'tasks'
    id = Column(Integer, primary_key=True, autoincrement=True)

    name = Column(Text, nullable=True)
    ended_at = Column(DateTime, default=fresh_timestamp())
    created_at = Column(DateTime, default=fresh_timestamp())
    status: TaskStatus = Column(Enum(TaskStatus), nullable=True)

    # tomato_sessions: List[TomatoSession] = relationship('TomatoSession', uselist=True)


class TaskToTomatoSession(Base):
    __tablename__ = 'tasks_to_tomato_sessions'
    id = Column(Integer, primary_key=True, autoincrement=True)
    task_id = Column(Integer, ForeignKey('tasks.id'))
    tomato_session_id = Column(Integer, ForeignKey('tomato_sessions.id'))
    created_at = Column(DateTime, default=fresh_timestamp())
