import datetime
import sys
import time
from typing import Optional, Union, Any

from PyQt5 import uic
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QLabel, QListWidget, QVBoxLayout, \
    QTabWidget

from interface.no_task_selected_modal import NoTaskSelectedModal
from interface.task_list import TaskList, TaskDeleteModal
from internal.settings import Settings
from internal.task import Task
from internal.tomato_session import TomatoSession
from models.models import TaskStatus, Task as TaskDb
from utils.utils import ToastNotifierMock, db_session


class TimerAppBase:
    """
    Вынесение объявлений для time hint'ов (stubs по своей сути)
    """
    tabWidget: QTabWidget

    resetTimerButton: QPushButton
    startTimerButton: QPushButton
    timerLabel: QLabel
    currentTaskLabel: QLabel
    timer: QTimer
    toaster: Union[ToastNotifierMock, Any]

    timer_interval: datetime.time  # интервал для помидора
    is_need_notificate: bool  # хочет ли пользователь получать уведомления
    is_timer_started: bool  # работает ли таймер в данный момент
    current_seconds_remain: float  # оставшееся время таймера в секундах
    start_time: float  # timestamp с момента начала отсчёта таймера
    current_time: float  # timestamp для отслеживания оставшегося времени

    tasksQueue: QWidget
    tasksQueueLabel: QLabel
    tasksQueueListWidget: QListWidget
    tasksQueueLayout: QVBoxLayout

    tasksInProgress: QWidget
    tasksInProgressLabel: QLabel
    tasksInProgressListWidget: QListWidget
    tasksInProgressLayout: QVBoxLayout

    tasksCompleted: QWidget
    tasksCompletedLabel: QLabel
    tasksCompletedListWidget: QListWidget
    tasksCompletedLayout: QVBoxLayout

    statusBarLabel: QLabel

    completeTaskButton: QPushButton


class TaskListLogic(TimerAppBase):
    def __init__(self):
        self.current_task: Optional[TaskDb] = None

        self.queue_list = TaskList(self, self.tasksQueue, self.tasksQueueLabel, self.tasksQueueListWidget,
                                   self.tasksQueueLayout, TaskStatus.queue,
                                   self.start_task,
                                   self.refresh_all)
        self.in_progress_list = TaskList(self, self.tasksInProgress, self.tasksInProgressLabel,
                                         self.tasksInProgressListWidget,
                                         self.tasksInProgressLayout, TaskStatus.in_progress,
                                         self.start_task,
                                         self.refresh_all)

        self.done_list = TaskList(self, self.tasksCompleted, self.tasksCompletedLabel,
                                  self.tasksCompletedListWidget,
                                  self.tasksCompletedLayout, TaskStatus.done, lambda x: None,
                                  self.refresh_all)

        self.completeTaskButton.clicked.connect(self.complete_task)
        self.tabWidget.tabBarClicked.connect(self.refresh_done_tomast_count)
        self.refresh_done_tomast_count(None)

    def refresh_done_tomast_count(self, tab):
        base_text = 'Всего сделано: '
        with db_session() as session:
            count = len(TomatoSession.get_list(session))
            self.statusBarLabel.setText(base_text + f'{count} помидоров')

    def complete_task(self):
        if self.current_task:
            with db_session() as session:
                Task.update(session, self.current_task.id, status=TaskStatus.done)
                self.current_task = None
                self.currentTaskLabel.setText('Текущая задача:')
                self.tabWidget.setCurrentIndex(0)
                self.refresh_all()
        else:

            dialog = NoTaskSelectedModal(self.tabWidget)
            dialog.exec_()

    def start_task(self, task: TaskDb):
        base_text = 'Текущая задача: "{name}"'
        self.tabWidget.setCurrentIndex(1)
        self.currentTaskLabel.setText(base_text.format(name=task.name))
        self.current_task = task

    def refresh_all(self):
        for task_list in [self.queue_list, self.in_progress_list, self.done_list]:
            task_list.refresh()


class TimerLogicMixin(TimerAppBase):
    """
    Логика для работы с таймером
    """

    def notify(self):
        if self.is_need_notificate:
            self.toaster.show_toast("Ваше время вышло", "Пора передохнуть", threaded=True)

    def update_time_label(self, time: Union[int, float]):
        self.timerLabel.setText(f'{str(int(time // 60)).rjust(2, "0")}:{str(int(time) % 60).rjust(2, "0")}')

    def show_time(self):
        new_time = time.time()
        delta = new_time - self.current_time
        self.current_time = new_time

        self.current_seconds_remain = self.current_seconds_remain - delta

        if self.current_seconds_remain <= 0:
            self.update_time_label(0)
            self.notify()
            self.timer.stop()
            self.is_timer_started = False
            self.startTimerButton.setText('Старт')
            with db_session() as session:
                TomatoSession.create(session, self.timer_interval.minute * 60 + self.timer_interval.second,
                                     datetime.datetime.fromtimestamp(self.start_time),
                                     datetime.datetime.fromtimestamp(self.current_time))
            return

        self.update_time_label(self.current_seconds_remain)

    def start(self):
        if not self.is_timer_started:
            if self.current_time is None:
                self.current_seconds_remain = self.timer_interval.minute * 60 + self.timer_interval.second

            self.is_timer_started = True
            self.current_time = time.time()
            self.start_time = time.time() if not self.start_time else self.start_time

            self.timer.start(1000)
            self.startTimerButton.setText('Остановить')
        else:
            self.is_timer_started = False
            self.startTimerButton.setText('Старт')
            self.timer.stop()

    def reset(self):
        self.timer.stop()

        self.is_timer_started = False
        self.startTimerButton.setText('Старт')
        self.current_seconds_remain: float = self.timer_interval.minute * 60 + self.timer_interval.second
        self.current_time = None
        self.start_time = None
        self.update_time_label(self.current_seconds_remain)


class TimerApp(QMainWindow, TimerLogicMixin, TaskListLogic):

    def __init__(self):
        QMainWindow.__init__(self)
        uic.loadUi('./ui/main.ui', self)
        TaskListLogic.__init__(self)

        self.startTimerButton.clicked.connect(self.start)
        self.resetTimerButton.clicked.connect(self.reset)

        self.timer = QTimer()
        self.timer.timeout.connect(self.show_time)

        self.load_settings()
        self.current_time = None
        self.start_time = None

        try:
            from win10toast import ToastNotifier
            self.toaster = ToastNotifier()
        except ImportError as e:
            self.toaster = ToastNotifierMock()

    # noinspection PyAttributeOutsideInit
    def load_settings(self):
        with db_session() as session:
            settings = Settings.get(session)
            self.timer_interval = datetime.time(minute=int(settings.tomato_duration // 60),
                                                second=int(settings.tomato_duration % 60))
            self.is_need_notificate = settings.is_need_notificate

            self.current_time: float = self.timer_interval.minute * 60 + self.timer_interval.second
            self.update_time_label(self.current_time)
            self.is_timer_started = False


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = TimerApp()
    ex.show()
    sys.exit(app.exec_())
