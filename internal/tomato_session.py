import datetime
from typing import List

from sqlalchemy import cast, Date
from sqlalchemy.orm import Session
from models.models import TomatoSession as DbTomatoSession


class TomatoSession:
    @staticmethod
    def get(session: Session, id: int) -> DbTomatoSession:
        return session.query(DbTomatoSession).get(id)

    @staticmethod
    def get_list(session: Session) -> List[DbTomatoSession]:
        query = session.query(DbTomatoSession)
        return query.all()

    @staticmethod
    def create(session: Session, duration: int,
               created_at: datetime.datetime, ended_at: datetime.datetime) -> DbTomatoSession:
        tomato = DbTomatoSession(duration=duration, created_at=created_at, ended_at=ended_at)
        session.add(tomato)
        session.commit()
        return tomato
