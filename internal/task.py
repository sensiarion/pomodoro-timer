import datetime
from typing import List, Optional

from sqlalchemy.orm import Session
from models.models import Task as DbTask, TaskStatus


class TooMuchTasksException(Exception):
    """
    Ошибка для остановки пользователя, на случай,
    если он захочет распланировать всю жизнь наперёд
    """
    pass


class Task:
    @staticmethod
    def get(session: Session, id: int) -> DbTask:
        return session.query(DbTask).get(id)

    @staticmethod
    def get_list(session: Session, status: TaskStatus) -> List[DbTask]:
        return session.query(DbTask).filter(DbTask.status == status).all()

    @staticmethod
    def create(session: Session, name: str, status: TaskStatus) -> DbTask:
        tomato = DbTask(name=name, status=status)
        session.add(tomato)
        session.commit()
        return tomato

    @staticmethod
    def update(session: Session, id: int, name: Optional[str] = None,
               status: Optional[TaskStatus] = None) -> DbTask:
        task = Task.get(session, id)
        if status:
            task.status = status
        if name:
            task.name = name
        session.commit()
        return task

    @staticmethod
    def delete(session: Session, id: int) -> DbTask:
        task = Task.get(session, id)
        session.delete(task)
        session.commit()
        return task
