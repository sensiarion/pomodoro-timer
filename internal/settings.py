from sqlalchemy.orm import Session

from models.models import Settings as DbSettings


class Settings:
    @staticmethod
    def get(session: Session) -> DbSettings:
        return session.query(DbSettings).get(1)

    @staticmethod
    def update(session: Session, tomato_duration: int, is_need_notificate: bool) -> DbSettings:
        settings: DbSettings = session.query(DbSettings).get(1)
        settings.tomato_duration = tomato_duration
        settings.is_need_notificate = is_need_notificate
        session.commit()
        return settings
