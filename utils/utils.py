from contextlib import contextmanager
from typing import Callable, Tuple, ContextManager

from sqlalchemy import orm
from sqlalchemy.engine import Engine, create_engine


class ToastNotifierMock:

    def show_toast(self, *args, **kwargs):
        pass


def session_factory():
    """
    Функция для создания фабрики соединений с бд.
    """
    engine = create_engine('sqlite:///database/database.db')
    # noinspection PyPep8Naming
    Session = orm.sessionmaker(bind=engine)

    @contextmanager
    def get_session() -> orm.Session:
        try:
            sess: orm.Session = Session()
            yield sess
        finally:
            sess.close()

    return get_session


db_session: Callable[[], ContextManager[orm.Session]] = session_factory()
